
export class DataOb {
  constructor(nodes, links) {
    nodes.forEach((node, i) => {
    node.selected = 0;
    if(node.id === undefined){ node.id = i; }
    if(node.cluster === undefined){ node.cluster = node.group; }
    });
    this.nodes = nodes;
    this.links = links;
  }
}

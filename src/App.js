import './App.css';
import React, { useState, useEffect } from 'react';
import rawData from "./data/data.json";

import { ContextProvider } from './ContextProvider.js';
import NodetrixMenu from "./nodetrix/NodetrixMenu.js";

import { DataOb }  from "./data/dataOb.js";


//App  handles data and loads nodetrix when ready.
function App() {
  const [data, setData] = useState(undefined);
  const [dataLoaded, setDataLoaded] = useState(false);
  const [rerender, setRerender] = useState(0);

  //inital dataload handler
  useEffect(() => {
    loadData();
  }, [])

  useEffect(() => {
    if(data !== undefined && data.nodes !== undefined && data.nodes !== undefined){
      document.getElementById('loadState').innerText = 'data ready';
      setDataLoaded(true);
    }
  }, [data])

  //Data retriever. has commented fetch included if to show how user would recieve data from a server.
  const loadData = () => {
    document.getElementById('loadState').innerText = 'loading';
     /*fetch('', {
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          }
        })
        .then( resp => resp.json())
        .then (resp => {setData(resp); console.log(resp)})
        .catch( error => console.log(error))
      })*/
      const dat = new DataOb(rawData.nodes, rawData.links);
      setData(dat);
      let counter = rerender;
      counter++;
      setRerender(counter);
  }

  return (
    <div className="App">
      <div id="AppContainer">
        { dataLoaded &&
          <ContextProvider>
            <NodetrixMenu data={data} key={rerender}/>
          </ContextProvider>
        }
      </div>
      <p id="loadState">
        loading data
      </p>
      <button onClick={() => loadData()}>
        reload data
      </button>
    </div>
  );
}

export default App;

import React, { useState, useMemo} from 'react';

export const SelectedContext = React.createContext();
export const HoverContext = React.createContext();

export function ContextProvider({ children }){
  const [selectedNodes, setSelectedNodes] = useState([]);
  const [hoverInfo, setHoverInfo] = useState(" ");

  const providerValue = useMemo(() => ({selectedNodes, setSelectedNodes}), [selectedNodes, setSelectedNodes]);
  const hoverValue = useMemo(() => ({hoverInfo, setHoverInfo}), [hoverInfo, setHoverInfo]);

  return(
    <SelectedContext.Provider value={ providerValue }>
      <HoverContext.Provider value={ hoverValue }>
        {children}
      </HoverContext.Provider>
    </SelectedContext.Provider>
  )
}

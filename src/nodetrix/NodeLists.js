import React from 'react';

// Method for sorting nodes into matrixes and nodes.
export class NoduleList {
    constructor(nodes, links) {
      this.nodes = nodes;
      this.links = links;
      this.nodeDict = {};
      this.matrixDict = {};
      this.CreateNetwork = this.CreateNetwork.bind(this);
      this.NewMatrixNum = 1;

      let tempNodeDict = {};
      //dictionary of nodes. good for searching up nodeElements.
      nodes.forEach((node, i) => {
        if( node.cluster === undefined )
        { node.cluster = 0; }
        tempNodeDict[node.id] = node;
      });
      this.nodeDict = tempNodeDict;

      // dictionary of matrixes with their nodes. links added later
      // create dictionary of matrixNodes
      let tempMatDict = {};
      nodes.forEach((node, i) => {
        if(node.cluster !== 0)
        {
          if(tempMatDict[node.cluster] === undefined){
            tempMatDict[node.cluster] = new MatrixOb(node, 0);
            if(this.NewMatrixNum <= node.cluster){this.NewMatrixNum = node.cluster+1};
          } else {
            let matrixNodes = tempMatDict[node.cluster].nodes;
            tempMatDict[node.cluster].nodes = [...matrixNodes, node];
          }
        }
      });
      this.matrixDict = tempMatDict;
    }

  // create a list of nodules and links in the network.
  CreateNetwork = () => {
    let network = {};
    var noduleList = [];
    var EdgeList = [];
    var numNodules = 0;

    // create nodules for the network.
    Object.values(this.nodeDict).forEach((node, i) => {
      let nodeOb = new NodeOb(node, numNodules);
      if(nodeOb.node.cluster === 0){
        noduleList.push(nodeOb);
        numNodules++;
      }
    });
    Object.values(this.matrixDict).forEach((matrixOb, i) => {
      matrixOb.id = numNodules;
      noduleList.push(matrixOb);
      numNodules++;
    });
    network.nodules = noduleList;
    // create links for the network.
    this.links.forEach((link, i) => {
      var sNode = this.nodeDict[link.source];
      var tNode = this.nodeDict[link.target];
      if( sNode.cluster !== undefined && sNode.cluster !== 0 && sNode.cluster === tNode.cluster ){
        this.matrixDict[sNode.cluster].links.push(link);  // add link to matrix if its connected to nodes in side the cluster.
      } else {
        let toMatrix = 0;                       // check if link sourceNode is nodule in network.
        let sourceNum = -1;                     // otherwise find matrix nodule of sourcenode of link.
        let targetNum = -1;                     // repeat for target Node.
        noduleList.forEach((noduleOb, i) => {
          if(noduleOb.type === "node"){
            if(sNode.cluster === 0){ if(noduleOb.node.id === sNode.id){ sourceNum = noduleOb.id; }}
            if(tNode.cluster === 0){ if(noduleOb.node.id === tNode.id){ targetNum = noduleOb.id; }}
          } else {
            if(sNode.cluster !== 0){ if(noduleOb.clusterNum === sNode.cluster){ sourceNum = noduleOb.id; toMatrix= toMatrix + 2;}}
            if(tNode.cluster !== 0){ if(noduleOb.clusterNum === tNode.cluster){ targetNum = noduleOb.id; toMatrix = toMatrix + 1;}}
          }
        });
        if(targetNum !== -1 && sourceNum !== -1){
          if(targetNum === undefined || sourceNum === undefined){ console.log(sourceNum); }
          let edgeOb = new EdgeOb(sourceNum, targetNum, link, toMatrix);
          EdgeList.push( edgeOb );
        }
      }
    });
    network.edges = EdgeList;
    // Add matrixNodules to network;
    return network;
  }
}

// creates object to store nodule data. will be extended for single node and matrix nodules.
class NoduleOb {
  constructor(id){
    this.id = id;
    this.x = 1;
    this.y = 1;
  }
}

class NodeOb extends NoduleOb{
  constructor(node, id) {
    super(id);
    this.type = "node";
    this.node = node;
  }
}

class MatrixOb extends NoduleOb{
  constructor(node, id) {
    super(id);
    this.type = "matrix";
    this.clusterNum = node.cluster;
    this.nodes = [ node ];
    this.links = [];
  }
}

class EdgeOb {
  constructor(source, target, link, toMatrix){
    this.source = source;
    this.target = target;
    this.link = link;
    //1 if source is from matrix, 2 if target is from matrix, 3 if both are, 0 if neither
    this.toMatrix = toMatrix;
  }
}
export default NoduleList

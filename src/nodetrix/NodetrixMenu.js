import React, { useState, useEffect, useContext} from 'react';
import NodeSimulation from './NodesNetwork.js';
import { NoduleList } from "./NodeLists.js";
import { SelectedContext, HoverContext } from './../ContextProvider.js';
import './NodetrixMenu.css';

// Function that combines menu with the simulation.
export function NodetrixMenu({data}) {
  const [width, setWidth] = useState("300");
  const [height, setHeight] = useState("300");
  //Nodetrix data
  const [nodes, setNodes] = useState(data.nodes);
  const [links, setLinks] = useState(data.links);
  // Nodules and edges are the nodes and the links in network and their visuals
  const [nodules, setNodules] = useState([]);
  const [edges, setEdges] = useState([]);
  // Context and States used for selecting and getting info of hovered nodes.
  const [matrixNum, setMatrixNum] = useState(0);
  const {selectedNodes, setSelectedNodes} = useContext(SelectedContext);
  const {hoverInfo} = useContext(HoverContext);
  // Two states that are used for rendering of child components.
  // One simply asks the components to rerender, the other to restart the nodeSimulation.
  const [rerender, setRerender] = useState(0);
  const [restart, setRestart] = useState(1);
  const [lockNodes, setLockNodes] = useState(false);

  // Changes nodes to be highlighted both visually and in data when selected
  useEffect(()=>{
    if(selectedNodes.length !== 0){
      // Changes nodes and selectedNodes list
      let newNodes = nodes;
      newNodes.forEach((node) => {
        if( selectedNodes.includes(node) ){ node.selected = 1; }
        else{ node.selected = 0; }
      });
      setNodes(newNodes);
      // calls rerender without restarting the simulation
      rerenderFunc(0);
    }
  }, [selectedNodes])

  // Creates Nodules and edges on startup
  useEffect(() => {
    setWidth(document.getElementById("canvasContainer").clientWidth);
    setHeight(document.getElementById("canvasContainer").clientHeight);
    CreateNodules(data.nodes);
  }, [])

  // Method for creating a list of nodules and edges data from the nodes and links.
  function CreateNodules(nodes){
    setNodes(nodes);
    setLinks(data.links);
    // Use NoduleList to create nodules List from nodes and matrixes.
    const noduleClass = new NoduleList(nodes, links);
    setMatrixNum(noduleClass.NewMatrixNum);
    const network = noduleClass.CreateNetwork();
    setNodules([...network.nodules]);
    setEdges([...network.edges]);
    // Call rerender simulation with new data
    rerenderFunc(1);
  }

  // method to set selected nodes to loose nodules
  function toNodes(){
    let newNodes = [...nodes];
    newNodes.forEach((node) => {
      if(selectedNodes.includes(node))
      { node.cluster = 0; return node; }
      else{ return node; }
    });
    CreateNodules(newNodes);
  }

  // method to set selected nodes to matrix
  function toMatrix(){
    let newNodes = [...nodes];
    newNodes.forEach((node) => {
      if(selectedNodes.includes(node)){
        node.cluster = (matrixNum);
        return node;
      }
      else{ return node; }
    });
    CreateNodules(newNodes);
  }

  // empty selected list to unselect all nodes.
  function RemoveSelection(){
    setSelectedNodes([]);
    let newNodes = nodes;
    newNodes.forEach((node, i) => {
      node.selected = 0;
    });
    setNodes(newNodes);
    rerenderFunc(0);
  }
  // select all nodes.
  function SelectAllHandler(){
    setSelectedNodes(nodes);
    rerenderFunc(0);
  }

  // use effect for locking all nodes into place and releasing them again
  const handleLock = (currentval) => {
    console.log(currentval)
    setLockNodes(!currentval);
    let nods = nodules;
    if(!currentval){
      nods.forEach((nod, i) => {
        nod.fx = nod.x;
        nod.fy = nod.y;
      });
      //this.force.stop();
      setNodules(nods);
    } else {
      nods.forEach((nod, i) => {
        nod.fx = undefined;
        nod.fy = undefined;
      });
      setNodules(nods);
    }
    rerenderFunc(0);
  }

  // function to rerender the simulation and menu,
  // Can be triggered with a parameter that determines if the simulation is restarted
  function rerenderFunc(restartVal){
    let re = restart + restartVal;
    setRestart(re);
    let counter = rerender;
    counter++;
    setRerender(counter);
  }
  const uploadPositions = (nodules) => setNodules(nodules);

  return(
  <div id="nodeTrix" key={rerender}>
    <div id="canvasContainer">
      {  (rerender && nodules !== undefined && nodules.length !== 0) &&
        <NodeSimulation nodules={nodules} edges={edges} width={width} height={height} numNodes={nodes.length} uploadPositions={uploadPositions}
          rerender={rerender} restart={restart} key={"nt:"+(rerender)} />
      }
    </div>
    <div id="menu" key={"menu"+selectedNodes.length}>
      <p> Hovering Over: </p>
      <div id="hoverInfo">
        <p> {hoverInfo} </p>
      </div>
      <p> Selected Nodes: </p>
      <div id="selectInfo">
        <button onClick={toNodes}>
          turn to Nodes
        </button>
        <button onClick={toMatrix}>
          turn to Matrix
        </button>
        <button onClick={()=>SelectAllHandler()}>
          Select all Nodes
        </button>
        <br/>
        <button onClick={RemoveSelection}>
          Cancel Selection
        </button>
        <label className="LockSwitch">
          <br/>
          <span className="slider"> lock/unlock all nodes: </span>
          <input type="checkbox" onChange={()=>handleLock(lockNodes)}  checked={lockNodes}/>
        </label>
        { selectedNodes.map((node, i) =>
          <li key={"selectedNode "+ node.id}> id: {node.id}  name: {node.name} </li>
        )}
      </div>
    </div>
  </div>
  )
}

export default NodetrixMenu

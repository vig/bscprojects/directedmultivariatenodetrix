import React, { useState, useContext } from 'react';
import MatrixGraphic from "./MatrixGraphic.js";
import { SelectedContext, HoverContext } from '../../ContextProvider.js';


// This method visualises all nodules.
const NodeGraphic = ({nodules, unitLength, rerenderprop}) => {
  const [ rerender] = useState(rerenderprop);

  const [ nodeNodules ] = useState(nodules.filter((node) => node.type === "node"  ) );
  const [ matrixNodules ] = useState( nodules.filter((node) => node.type === "matrix") );
  const {selectedNodes, setSelectedNodes} = useContext(SelectedContext);
  const { setHoverInfo} = useContext(HoverContext);

  function SelectNode(node){
    if(!selectedNodes.includes(node))
    {
      var sc = [...selectedNodes];
      sc.push(node);
      setSelectedNodes(sc);
    }
  }

  return (
  <g>
    { nodeNodules.map((nodule, index) =>
      <circle key={"node: "+nodule.id}
        className="nodule"
        id={nodule.id}
        cx={nodule.x}
        cy={nodule.y}
        r={unitLength/5}
        stroke={"rgb("+(255*nodule.node.selected)+",0,0)"}
        strokeWidth = {(nodule.node.selected*2) + 1}
        fill="black" //change fill with type later.
        onClick={()=>SelectNode(nodule.node)}
        onMouseEnter={() => setHoverInfo("Node: "+nodule.node.name+" Node Id: "+nodule.node.id)}
        onMouseLeave={() => setHoverInfo("")}
      />
    ) }
    {matrixNodules.map((nodule, index) =>
      <React.Fragment key = {index} >
        <MatrixGraphic matrix={nodule} unitLength={unitLength} SelectNode={SelectNode} rerender={rerender} setHoverInfo={setHoverInfo} key={nodule.id+ " " +rerender}/>
        <rect key={nodule.id}
          id={nodule.id}
          className="nodule"
          x={nodule.x-(unitLength*nodule.nodes.length/10)}
          y={nodule.y-(unitLength*nodule.nodes.length/10)}
          width={unitLength*nodule.nodes.length/5}
          height={unitLength*nodule.nodes.length/5}
          fill="transparent"
        />
      </React.Fragment>
    )}
  </g>
  )
}

export default NodeGraphic

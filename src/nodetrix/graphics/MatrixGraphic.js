import React, { useState, useEffect } from 'react';

// all gridsizes are 1.4 * (nodes/5) *unitlength long
// This method constructs the visuals for a single matrix.(Called from NoduleGraphic)
const MatrixGraphic = ({matrix, unitLength, SelectNode, setHoverInfo, rerender}) => {
  const [nodes] = useState(matrix.nodes);
  const [links] = useState(matrix.links);
  const [gridsize] = useState(unitLength/5);
  const [totalgridsize] = useState(unitLength*matrix.nodes.length/5);
  const [linkGrid, setLinkGrid] = useState([]);

  useEffect(()=>{
  // SOURCE https://bl.ocks.org/harrisoncramer/32bbcd8c360e6d8aa0d5b7a50725fe73
    var linkDict = {};
    var directed = false;

    links.forEach((link) => {
      var id = link.source + "-" + link.target;
      linkDict[id] = link;
    });

    // if undirected clone link into dictionary with oposite source and target
    if(!directed){
      links.forEach((link) => {
        var id = link.target + "-" + link.source;
        linkDict[id] = link;
      });
    }

    var gridForLinks = []
    nodes.forEach((source, xVal) => {
    	nodes.forEach((target, yVal) => {
    		var square = {id: source.id + "-" + target.id, x: xVal, y: yVal, weight: 0};
        // check for link
        if(target.id === source.id){ square.weight = 0.5;}
    	  if(linkDict[square.id]){ square.weight = 1; }
        // check for selected node
        if(target.selected === 1 || source.selected === 1){square.stroke = "red";}
        else{square.stroke = "rgba(0, 0, 0)";}
    	  gridForLinks.push(square);
    	})
    });
    setLinkGrid(gridForLinks);
    }, [matrix])

    return(
      <g>
        { (!isNaN(matrix.x) && !isNaN(matrix.y)) && /* grid backgound color */
          <svg className="nodule"
            id={matrix.id}
            width = {unitLength * 1.4 + totalgridsize}
            height = {unitLength * 1.4 + totalgridsize}
            x = {matrix.x - (unitLength*1.4 + totalgridsize)/2}
            y = {matrix.y - (unitLength*1.4 + totalgridsize)/2}
            fill = "#e8f4f8"
            fillOpacity = "1"
          >
            { linkGrid.map((square, i) => // Creating the grid that show the links
              <rect key={"square:"+square.id+" "+matrix.id}
                className="grid"
                width = {gridsize}
                height = {gridsize}
                x = {square.x * gridsize + unitLength*.7}
                y = {square.y * gridsize + unitLength*.7}
                fill = {"rgb("+ (1-square.weight)*255  +","+ (1-square.weight)*255 +",255)"}
                stroke = {square.stroke}
                strokeWidth = ".4"
              />
            )}
            { nodes.map((node, i) => // Creating the node descriptions at the sides
              <g
                key={"label:"+node.id+" "+matrix.id}
                onClick={()=>{SelectNode(node)}}
                onMouseEnter={() => setHoverInfo("Node: "+node.name+" Node Id: "+node.id)}
                onMouseLeave={() => setHoverInfo("")}>
                <NodeDescSquare i={i} node={node} matrix={matrix}
                  gridsize={gridsize} totalgridsize={totalgridsize} unitLength={unitLength} key={"label:"+node.id+" "+matrix.id}/>
              </g>

            )}
          </svg>
        }
      </g>
    )
}

      //const position = "translate(" + unit + ", " + unit +")" ;
      /**svg.selectAll("rect.grid").on("mouseover", gridOver);
      function gridOver(d) {
        d3.selectAll("rect").style("stroke-width", function(p) { return p.x === d.x || p.y === d.y ? "3px" : "1px"});
      };**/

function NodeDescSquare({i, node, matrix, gridsize, totalgridsize, unitLength}){
  const [strokeColor, setStrokeColor] = useState("black");
  const [strokeWidth, setStrokeWidth] = useState(".2");
  const [labelLength] = useState(unitLength*.7);

  useEffect(()=>{
    if(node.selected === 1){setStrokeColor("red");setStrokeWidth("1");}
    else{setStrokeColor("black");setStrokeWidth(".1");}
  }, [node])

  // renders id name first and then the box
  return(
      /*  top box and text  */
        <>
          <rect
            width = {gridsize}
            height = {labelLength}
            x = {(i*gridsize)+labelLength}
            y = "0"
            fill = "#ffffba"
            stroke = {strokeColor}
            strokeWidth = {strokeWidth}
          />
          <text
            fontWeight="bold"
            textAnchor="middle"
            fontSize={gridsize/2}
            width = {gridsize}
            height = {labelLength}
            transform={ "translate(" + (i*gridsize+labelLength) +
              ", " + (1.5 * gridsize) + ") rotate(90)"
            }
            fill = "black"
          > {node.name}
          </text>
          { /*  bottom box and text  */ }
          <rect
            width = {gridsize}
            height = {labelLength}
            x = {labelLength + i*gridsize}
            y = {labelLength + totalgridsize}
            fill = "#ffffba"
            stroke = {strokeColor}
            strokeWidth = {strokeWidth}
          />
          <text
            fontWeight="bold"
            textAnchor="middle"
            fontSize={gridsize/2}
            width = {gridsize}
            height = {gridsize * 2}
            transform={"translate(" + (i*gridsize+labelLength) +
              ", " + (totalgridsize + 1.5 * labelLength) + ") rotate(90)"
            }
            fill = "black"
          >
            {node.name}
          </text>

          { /*  left box and text  */ }
          <rect
            width = {labelLength}
            height = {gridsize}
            x = "0"
            y = {i*gridsize+labelLength}
            fill = "#ffffba"
            stroke = {strokeColor}
            strokeWidth = {strokeWidth}
          />
          <text
            fontWeight="bold"
            fontSize={gridsize/2}
            textAnchor="middle"
            width = {gridsize}
            height = {gridsize * 2}
            x = {labelLength/2}
            y = {(i + 1)*gridsize + labelLength}
            fill = "black"
          >
            {node.name}
          </text>

          { /*  right box and text  */ }
          <rect
            width = {labelLength}
            height = {gridsize}
            x = {labelLength + totalgridsize}
            y = {i*gridsize+labelLength}
            fill = "#ffffba"
            stroke = {strokeColor}
            strokeWidth = {strokeWidth}
          />
          <text
            fontWeight="bold"
            fontSize={gridsize/2}
            textAnchor="middle"
            height = {gridsize * 2}
            x = {labelLength*1.5 + totalgridsize}
            y = {(i+1)*gridsize+labelLength}
            fill = "black"
          >
            {node.name}
          </text>
        </>
  )
}



export default MatrixGraphic
/*
function MatrixGraphic(nodes, links, size) {
  const center = [size/2, size/2];
  const unit = size / (nodes.length * 5);
  const position = "translate(" + unit + ", " + unit +")" ;
  // SOURCE https://bl.ocks.org/harrisoncramer/32bbcd8c360e6d8aa0d5b7a50725fe73
  const svg = d3.select('svg');
  var linkSquare = {};
	links.forEach(link =>{
			var id = link.source + "-" + link.target;
			linkSquare[id] = link;
		});

  var matrix = []
		nodes.forEach((source, a) => {
			nodes.forEach((target, b) => {
				var grid = {id: source.id + "-" + target.id, x: b, y: a, weight: 0};
				if(linkSquare[grid.id]){
					grid.weight = 1;
				}
			matrix.push(grid);
			})
		});

    svg.append("g").attr("transform", position)
  		.attr("id","adjacencyG")
  		.selectAll("rect")
  		.data(matrix)
  		.enter()
  		.append("rect")
  		.attr("class","grid")
  		.attr("width", unit)
  		.attr("height", unit)
  		.attr("x", d=> d.x * unit)
  		.attr("y", d=> d.y * unit)
  		.style("fill-opacity", d=> d.weight * .2);

    svg.append("g")
      .attr("transform",position)
  		.selectAll("rect")
  		.data(nodes)
  		.enter()
      .append('rect')
      .attr("x", (d,i) => i * unit + unit/2)
      .attr("width", (size/3) / nodes.length)
      .attr("height", (size/2.5) / nodes.length)
      .attr("fill", "yellow")
  		.append("text")
  		.text(d => d.id)
  		.style("text-anchor","middle")
  		.style("font-size","10px");

  	svg.append("g")
  		.attr("transform",position)
  		.selectAll("text")
  		.data(nodes)
  		.enter()
  		.append("text")
  		.attr("x", (d,i) => i *  unit + unit/2)
  		.text(d => d.id)
  		.style("text-anchor","middle")
  		.style("font-size","10px");

	 svg.append("g").attr("transform",position)
      .attr("transform",position)
  		.selectAll("text")
  		.data(nodes)
  		.enter()
  		.append("text")
  		.attr("y",(d,i) => i *  unit + unit/2)
  		.text(d => d.id)
  		.style("text-anchor","end")
  		.style("font-size","10px");

}

export default MatrixGraphic
*/

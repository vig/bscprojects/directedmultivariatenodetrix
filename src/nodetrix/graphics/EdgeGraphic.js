import React, { useState} from 'react';

// method that renders all edges in the network visualisation.
function EdgeGraphic({ edges, matrixNodes, unitLength }) {
  const nodeEdges = edges.filter((edge) => edge.toMatrix === 0) // edges between nodes modules (straight)
  const matrixEdges = edges.filter((edge) => edge.toMatrix > 0)   //edges that link to matrix nodules (curved)
  return (
    <g>
      {nodeEdges.map((edge, i) =>
        <line key= {"edgegraphic"+ edge.source + " - " + edge.target + " " + i}
          data={edge}
          x1={edge.source.x}
          y1={edge.source.y}
          x2={edge.target.x}
          y2={edge.target.y}
          stroke="black"
          strokeWidth="1"
        />
      )}

      {matrixEdges.map((edge, i) =>
        <React.Fragment key={"matrixEdge" + i} >
          { (!isNaN(edge.source.x) && !isNaN(edge.target.y)) &&
            <CreateMatrixEdge edge={edge} unitLength={unitLength} matrixNodes={matrixNodes} key={edge.source.x+" "+edge.source.y+" "+edge.target.x+" "+edge.target.y}/>
          }
        </React.Fragment>
      )}
    </g>
    )
}

// Method for creating graphics for links connected to matrix nodules.
const CreateMatrixEdge = ({edge, unitLength, matrixNodes}) => {
  const [sourceX, setSourceX] = useState(edge.source.x);
  const [sourceY, setSourceY] = useState(edge.source.y);

  const [Ydiff, setYdiff] = useState(0);
  const [Xdiff, setXdiff] = useState(0);

  const [targetX, setTargetX] = useState(edge.target.x);
  const [targetY, setTargetY] = useState(edge.target.y);

  useState(() => {
    const  XYdiff = setSide();
    setXdiff(XYdiff[0]);
    setYdiff(XYdiff[1]);
    const gridsize = unitLength/5;
    if(edge.toMatrix !== 1){ //2 and 3
      //find source matrix nodule
      const sourceMat = matrixNodes.find((matrix) => matrix === edge.source);
      //finde index of node in matrix
      const sourceGridNum = sourceMat.nodes.findIndex((node)=> node.id === edge.link.source );
      //const [gridsize] = useState(unitLength/5);
      //const [totalgridsize] = useState(unitLength*matrix.nodes.length/5);
      //get values for edge starting location

      const boxsizeS = (unitLength*.7+gridsize*sourceMat.nodes.length/2);
      const iPositionS = (gridsize * ((sourceGridNum-sourceMat.nodes.length/2)+.5)  );

      if( XYdiff[0] !== 0 ){ setSourceX(edge.source.x + (boxsizeS * XYdiff[0]) ); }
      else{ setSourceX(edge.source.x + iPositionS); }
      if( XYdiff[1] !== 0 ){ setSourceY(edge.source.y + (boxsizeS * XYdiff[1]) ); }
      else{ setSourceY(edge.source.y + iPositionS); }
    }
    if(edge.toMatrix !== 2){// 1 and 3
      const targetMat = matrixNodes.find((matrix) => matrix === edge.target);
      var targetGridNum = targetMat.nodes.findIndex((node) => node.id === edge.link.target );

      const boxsizeT = (unitLength*.7+gridsize*targetMat.nodes.length/2);
      const iPositionT = (gridsize * ((targetGridNum-targetMat.nodes.length/2)+.5)  );

      if( XYdiff[0] !== 0 ){ setTargetX(edge.target.x + (boxsizeT * -XYdiff[0]) ); }
      else{ setTargetX(edge.target.x + iPositionT); }
      if( XYdiff[1] !== 0 ){ setTargetY(edge.target.y + (boxsizeT * -XYdiff[1]) ); }
      else{ setTargetY(edge.target.y + iPositionT); }
    }
  }, [edge, matrixNodes])

  function setSide() {
    var XYdiff;
    //calculate differences between x and y of targets.
    var XSourceToTarget = edge.target.x - edge.source.x;
    var YSourceToTarget = edge.target.y - edge.source.y;
    // check if the difference is horizontal or vertical.
    if(XSourceToTarget**2 > YSourceToTarget**2){
    // calculate if the X difference is negative or positive
      if(XSourceToTarget > 0){XYdiff = [1,0];}
      else{XYdiff = [-1,0];}
    } else {
    // calculate if the Y difference is negative or positive
      if(YSourceToTarget > 0){XYdiff = [0, 1];}
      else{XYdiff = [0,-1];}
    }
    return XYdiff;
  }


return(
  <React.Fragment key={edge.source.x}>
    {(!isNaN(sourceX) && !isNaN(sourceY) && !isNaN(targetX) && !isNaN(targetY)) &&
      <path
        data={edge}
        d={
          "M " +
          //starting point
          sourceX + " " +
          sourceY + " " +
          "C " +
          //control point 1
          (sourceX + (unitLength * 2 *Xdiff)) + " " +
          (sourceY + (unitLength * 2 * Ydiff)) + ", " +
          //control point 2
          (targetX - (unitLength * 2 * Xdiff)) + " " +
          (targetY - (unitLength * 2 * Ydiff)) + ", " +
          //end point
          targetX + " " +
          targetY
        }
        stroke="black"
        strokeWidth="1"
        fill="transparent"
      />
    }
  </React.Fragment>
)
}

export default EdgeGraphic

/**const link = d3.select("svg").append("g")
  .selectAll('line')
  .data(edges)
  .join('line')
  .style("stroke", "black")
  .attr("stroke-width", 1);**/

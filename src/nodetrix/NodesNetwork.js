import React, { useState, useEffect} from 'react';
import * as d3 from "d3";
//import NodeGraphic from "./objects/NodeGraphic.js";
import NodeGraphic from "./graphics/NoduleGraphic.js";
import EdgeGraphic from "./graphics/EdgeGraphic.js";


// NodeSimulation creates and renders the visualisation simulation.
// For this, we use d3.forceSimulation
function NodeSimulation(props) {
  // standard unit length of our visualisation. Changes depending on canvas size and amount of nodes.
  const [unitLength] = useState( ( Math.min(props.width,props.height) /(2.5 * Math.sqrt(props.numNodes)) ) );
  const [rerender] = useState(props.rerender);
  // Nodes in network and their visuals
  const [nodules, setNodules] = useState(props.nodules);
  // Links in network and their visuals
  const [edges, setEdges] = useState(props.edges);
  // UseEffect that creates the network value changes

  useEffect(() => {
    const center = [props.width/2, props.height/2];
    const svg = d3.select('svg').style("background", "white").attr('viewBox', '0 0 ' + props.width + ' ' + props.height);
    // create simulation with nodules as nodes
    const NodeSimulation = d3.forceSimulation().nodes(nodules);

    //Simulation forces
    NodeSimulation.force('center', d3.forceCenter(center[0], center[1]))
                  .force('charge', d3.forceManyBody().strength(-10))
                  .force('link', d3.forceLink(edges)
                                .distance(function(d){if(d.toMatrix>0){return (unitLength * 5)}else{return (unitLength * 3)} }).strength(1))
                  .force('collide', d3.forceCollide().radius(function(d){
                      if(d.type==="matrix"){return (unitLength*(d.nodes.length/5+1)) }else{return unitLength;}
                    }));

    NodeSimulation.alphaDecay(0.03)         // slow down force over time.
                  .on("tick", ticked);      // set the data and properties of node circles.

    // call the function that handles the drag functions and add them to the node elements
    handleDrag(NodeSimulation, svg)
  }, [props.restart])

  // update state on every iteration
  function ticked(){
      setNodules([...nodules]);
      setEdges([...edges]);
      // send node positions back up to the menu for locking in place
      props.uploadPositions([...nodules]);
  }

  // function that contains all methods for draging nodules
  function handleDrag(NodeSimulation, svg){
    // an empty state that is used when dragging a nodule
    var draggedEl = undefined;
    var drag = d3.drag()
              .on("start", dragStart)
              .on("drag", dragged)
              .on("end", dragEnd);

    function  dragStart(event){
      NodeSimulation.alphaTarget(0.3).restart();
      var node = this;
      draggedEl = node.id;
      document.body.style.cursor = 'grabbing';
    }

    // drag element with mouse, but make sure it stays within svgElement
    function  dragged(event) {
      var newNods = nodules;
      newNods[draggedEl].fx=Math.max(1, Math.min(event.x, props.width-1));
      newNods[draggedEl].fy=Math.max(1, Math.min(event.y, props.height-1));
      setNodules(newNods);
    }

    function  dragEnd(event, d) {
      NodeSimulation.alphaTarget(0);
      draggedEl = undefined;
      document.body.style.cursor = 'default';
    }

    svg.selectAll(".nodule")
          .call(drag);
  }


  // renders all the nodules and edges in the canvas svg
  return(
      <svg id="canvas" width="100%" height="100%">
        { ( edges !== undefined && edges.length !== 0) &&
          <EdgeGraphic edges={edges} matrixNodes={nodules.filter((node) => node.type === "matrix")} unitLength={unitLength} rerender={rerender} key={"edges:"+rerender}/>
        }
        {( nodules !== undefined && nodules.length !== 0) &&
          <NodeGraphic nodules={nodules} unitLength={unitLength}  rerender={rerender} key={"nodules:"+rerender}/>
        }
      </svg>
  )
}


export default NodeSimulation
